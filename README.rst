Overview
========

Simple Converter Currency Exchange " โปรแกรมคำนวณอัตราการแลกเปลี่ยนสกุลเงิน "


Features
========

- Converter currency exchange "แปลงค่าสกุลเงินได้"
- View rates in each country "สามารถดูอัตราการแลกเปลี่ยนของแต่ละประเทศได้"


Use cases
=========

Converter currency exchange "แปลงค่าสกุลเงินได้"
------------

#. go to main page "เข้าหน้าหลัก"
#. enter a value in a textbox "ใส่จำนวนเงินที่ต้องการคำนวณ"
#. select currency exchange "เลือกสกุลเงินที่ต้องการ"
#. click convert "คลิ๊กปุ่ม แปลงสกุลเงิน (Convert)"


View rates in each country "สามารถดูอัตราการแลกเปลี่ยนของแต่ละประเทศได้"
----------

#. go to main page "เข้าหน้าหลัก"
#. click view rates display "คลิ๊กปุ่ม ดูการแลกเปลี่ยนของแต่ละประเทศ (Rates)"